import org.junit.Test;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static junit.framework.Assert.assertEquals;

public class JavaExampleTests {

    @Test
    public void lists() {
        assertEquals(newArrayList(1, 2), newArrayList(1, 2));
    }
}
