package warsjawa2012.spockogroovy

import spock.lang.Specification

import java.util.regex.Pattern
import spock.lang.Ignore

class Ex02AboutStringsSpecification extends Specification {

    def 'single and double quotes can be used'() {

        given:
        String name = 'George'
        String lastname = "Smith"

        expect:
        name instanceof java.lang.String
        lastname instanceof java.lang.String
    }




    @Ignore('exercise: assing value to the message variable with a GString notation')
    def 'comfortable variables concatenation'() {

        given:
        String login = 'george79'

        when:
        def message // hint: use $variable notation and double-quotes

        then:
        message == 'User george79 has logged in'
        message instanceof groovy.lang.GString
        // more about: http://groovy.codehaus.org/Strings+and+GString
    }




    def 'concatenate with expression'() {

        given:
        int a = 5
        int b = 3

        when:
        def message = "Sum of $a and $b equals ${ a + b }" // any expression or method call can be placed here

        then:
        message == 'Sum of 5 and 3 equals 8'
    }




    @Ignore('exercise: assign value to the message variable with a GString notation')
    def 'concatenate with another expression'() {

        given:
        String dateFormat = 'yyyy-MM-dd HH:mm'
        Date date = Date.parse(dateFormat, '2007-10-23 12:12')

        when:
        def message // hint: use Date.format(String) method

        then:
        message == 'Uptime since 2007-10-23 12:12 and still working'
    }




    def 'multiline string'() {

        given:
        def multilined = '''1st line
2nd line
3rd line
4th line'''

        expect:
        multilined == '1st line\n2nd line\n3rd line\n4th line'
        multilined instanceof String
    }




    @Ignore('exercise: assign GString multiline value to the message variable with three double-quotes')
    def 'multiline string with dynamic part'() {

        given:
        String subject = 'shop'
        String login = 'Martinezz'
        int counter = 15

        when:
        def multilined

        then:
        multilined == '\nWelcome to our shop\nThank you Martinezz for visiting us 15 times\nHave a nice day!\n'

    }




    def 'opposite to split - concatenate collection (aka join)'() {

        given:
        def aList = ['And', 'the', 1, 'ring', 'to', 'rule', 'them', 'all']

        when:
        def concatenatedWithDash = aList.join('-')

        then:
        concatenatedWithDash == 'And-the-1-ring-to-rule-them-all'
    }




    @Ignore('exercise: assing value to the windowsPath variable using slashes instead of quotes to avoid escaping')
    def 'slashy strings to avoid escaping'() {

        given:
        String oldEscapedPath = 'C:\\windows\\system32'

        when:
        def windowsPath

        then:
        windowsPath == oldEscapedPath
    }




    def 'slashy strings are handy for regexps'() {

        given:
        String testString = "${System.currentTimeMillis()} foo ${System.currentTimeMillis()}"

        when:
        Pattern pattern = ~/\d+ foo \d+/

        then:
        testString.matches(pattern)
    }




    @Ignore('exercise: assign value to the multiplies variable with * operator')
    def 'string multiplication'() {

        given:
        int times = 5
        String str = 'ha'

        when:
        def multiplied

        then:
        multiplied == 'hahahahaha'
    }




    @Ignore('exercise: assign value to the reversed variable')
    def 'reversing a string'() {

        given:
        String aString = '1234567890'

        when:
        def reversed // hint: just call a proper method on string

        then:
        reversed == '0987654321'
    }




    def 'toBoolean GDK Goodness'() {

        expect:
        'y'.toBoolean()
        'TRUE'.toBoolean()
        '  trUe  '.toBoolean()
        ' y'.toBoolean()
        '1'.toBoolean()

        !'other'.toBoolean()
        !'0'.toBoolean()
        !'no'.toBoolean()
        !'  FalSe'.toBoolean()
    }
}
