package warsjawa2012.spockogroovy

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Ignore

class Ex03GroovyTruthSpecification extends Specification {




    @Shared def falsyValues =  [ [],   [:] ,           '',       0, 0.0,   null ]
    @Shared def truthyValues = [ [42], [key: 'value'], 'bacon', -1, 0.001, new Greeter() ]





    def 'empty lists, maps, strings, zeros and null are falsy'() {
        expect:
        'is falsy'(value)

        where:
        value << falsyValues
    }

    def 'non-empty lists, maps, strings, non-zeros and other non-nulls are truthy'() {
        expect:
        'is truthy'(value)

        and: 'Spock uses Groovy truth as well'
        value

        where:
        value << truthyValues
    }



    def 'being truthy is not the same as being true'() {
        expect:
        !(value == true)
        value != true

        where:
        value << truthyValues
    }

    def 'being falsy is not the same as being false'() {
        expect:
        !(value == false)
        value != false

        where:
        value << falsyValues
    }



    def 'negation operator converts falsies to true (of type Boolean)'() {
        expect:
        (!value) instanceof Boolean

        where:
        value << falsyValues
    }

    def 'negation operator converts truthies to false (of type Boolean)'() {
        expect:
        (!value) instanceof Boolean

        where:
        value << truthyValues
    }

    void 'is falsy'(def value) {
        if (value) {
            assert !value
        }
    }

    void 'is truthy'(def value) {
        if (value) {
            //well, that's ok :) (avoiding the '!' operator - one of the features explains why. Which?)
        } else {
            assert value
        }
    }

    /*
    QUIZ:
     - what other Groovy's syntax elements could you think of, where Groovy truth might sometimes help?
    */
    @Ignore('exercise')
    def 'other syntax elements usage'() {
        expect:
        false //exercise








    }
}
