package warsjawa2012.spockogroovy

import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

// ref: http://code.google.com/p/spock/wiki/SpockBasics
class Ex00SpockBasicsSpecification extends Specification {







    def 'a passing test'() {
        expect:
        1 == 1
    }






    @Ignore
    def 'a failing test, ignored so that all tests pass'() {
        when:
        def one = 1

        then:
        one == 1 + 3
    }






    @Ignore
    def "Spock's assertion error messages are truly awesome"() {
        expect:
        ['foo', 'bar', 'baz'] == ['foo', 'kaboom', 'bar', 'baz', 'pif paf']
    }










    def personToGreet = 'Mr. Spock'

    def 'greeting Mr. Spock properly (verified using a helper method)'() {
        given:
        def greeter = new Greeter()

        when:
        def greeting = greeter.greet(personToGreet)

        then:
        isProperGreeting(greeting)
    }

    void isProperGreeting(String greeting) {
        assert greeting == 'Hello, ' + personToGreet + '!'
    }

    def 'Spock remembers old values of expressions that you care about'() {
        given:
        def list = [1, 2, 3]

        when:
        list.add(4)

        then:
        list.size() == old(list.size()) + 1 //magic :)
    }




    def "length of Spock's and his friends' names"() {
        expect:
        name.size() == length

        where:
        name     | length
        'Spock'  | 5
        'Kirk'   | 4
        'Scotty' | 6
    }




    @Unroll('"#name" should be #length letter(s) long and start with #firstLetter')
    def "length of Spock's and his friends' names and their first letter"() {
        expect:
        name.size() == length
        name.startsWith(firstLetter)

        where:
        name     | length   | firstLetter  
        'Spock'  | 5        | 'S'       
        'Kirk'   | 4        | 'K'      
        'Scotty' | 6        | 'S'       
    }




    def 'Spock blocks can have textual descriptions'() {
        given: /an empty bank account/
        def account = 0

        when: /the account is credited $10'/
        account += 10

        then: /the account's balance is $10/
        account == 10
    }

    /*
    Exercises:
    - comment out the @Ignore-s, run the tests and check out the kickass-uber-readable assertion failed messages!
      - make sure to 'Click to see difference' in IDEA! :)
    - do the same for the 'greeting Mr. Spock' test

    - write your own Spock feature describing that
        'given strings 'foo' and 'bar', when concatenating them, the result contains 'ooba' as a substring'
      - try to layout the test with various labels. Find the way you like best for this test.
      - try omitting some labels (e.g. try using given: then: only)
      - make the test fail and then remove all the labels. Then run all tests. What happened?
    */

    /* FIXTURE METHODS */
    def setupSpec() { /* executed once before all feature methods */ }
    def setup() { /* executed at the beginning of every feature method */ }
    def cleanup() { /* executed after every feature method */ }
    def cleanupSpec() { /* executed once after all feature methods */ }

    /*
    Exercises:
    - fix the 'greeting Mr. Spock' test, change 'void' to 'def' in isProperGreeting, run the test, see what happens
    - then remove the assert in isProperGreeting and run the test
    - then break the test again. Why is the def-return-boolean helper method approach worse than the void-assert one?
    */

    /*
    Fun fact:
     Groovy has incorporated Spock's error reporting capabilities into the assert keyword! (since Groovy 1.7)
    */

}
