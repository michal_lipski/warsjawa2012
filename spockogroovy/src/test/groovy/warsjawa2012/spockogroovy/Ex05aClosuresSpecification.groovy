package warsjawa2012.spockogroovy

import spock.lang.Specification
import spock.lang.Unroll
import java.awt.event.ActionListener
import java.awt.event.ActionEvent
import spock.lang.Ignore

class Ex05aClosuresSpecification extends Specification {

    def alice = new Person('Alice', 10)
    def bob = new Person('Bob', 20)
    def carol = new Person('Carol', 30)
    def dave = new Person('Dave', 20)

    def people = [alice, dave, bob, carol]

    /*
    - this could be written shorter a bit shorter using 'it'
     */
    def 'finding an element in a collection'() {
        when:
        def person = people.find { suspect -> suspect.name.contains('ice') }

        then:
        person == alice
    }

    /*
    - this closure takes one argument - 'it' (here unused)
    - sometimes one can see
        closure = { -> 'some return value' } syntax - it simply means a 'zero-arg closure'
     */
    def 'closure is a fragment of code - at your fingertips!'() {
        given:
        def isGood = true
        def breakThings = { isGood = false }

        expect:
        isGood

        when:
        breakThings()

        then:
        !isGood
    }

    def 'closures can be coerced into interfaces'() {
        given:
        def button = new Button()
        def controller = new Controller()
        def executedCommand = null

        and: 'a plain java listener'
        button.addClickListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent event) {
                controller.handleClick(event)
            }
        })

        and: 'a closure coerced into an interface with explicit conversion'
        button.addClickListener({ event -> executedCommand = event.actionCommand } as ActionListener)

        and: 'and another one'
        button.addClickListener({ controller.handleClick(it) } as ActionListener)

        and: 'a method closure coerced into the interface'
        button.addClickListener(controller.&handleClick as ActionListener)

        when: 'user clicks'
        button.click()

        then: 'which do you prefer? :)'
        executedCommand == 'click'
        controller.clicks == 3
    }

    /*
   - could generate addClickListener method along with some more, here not necessary
   - in general, Groovy can generate quite a lot code for you (like Project Lombok)
    */
    class Button {

        List<ActionListener> listeners = []

        def addClickListener(ActionListener listener) {
            listeners << listener
        }

        def click() {
            listeners*.actionPerformed(new ActionEvent(this, i++, 'click'))
        }

        def i = 0
    }

    class Controller {
        def clicks = 0

        def handleClick(ActionEvent e) {
            clicks++
        }
    }

    def 'coercing a string into an enum'() {

        when: 'doing valid explicit and implicit coercions'
        def explicitlyCoerced = 'sushi' as Food
        Food implicitlyCoerced = 'steak'

        then:
        explicitlyCoerced == Food.sushi
        implicitlyCoerced == Food.steak

        when: 'attempting an invalid coercion'
        Food food = 'plants'

        then:
        def exception = thrown(IllegalArgumentException)
        exception.message.contains 'No enum constant '
        exception.message.contains 'Food.plants'
    }

    enum Food { sushi, steak, pasta }

    def 'general statements about collection elements'() {
        expect:
        people.every { it.age > 7 }
        people.any { it.age < 18 }
    }

    def 'collecting (a.k.a. *map*ping / *transform*ing)'() {
        expect:
        people.collect { it.age } == [10, 20, 20, 30]

        and:
        (1..4).collectEntries { [it, it ** 2] } == [1: 1, 2: 4, 3: 9, 4: 16]

        and:
        def nested = [[1], [[2], [3, 4], [5]]]
        def square = { it ** 2 }
        nested.collectNested(square) == [[1], [[4], [9, 16], [25]]]
    }

    /*
    Exercise
     fill in the gaps :)
     */
    @Ignore('exercise')
    def 'merging people with age'() {
        given:
        def peopleNames = ['Alice', 'Bob', 'Carol', 'Dave']
        def peopleAges = [10, 20, 30, 20]
        def people = people.sort { it.name }
        def expectedNameAgePairs = [['Alice', 10], ['Bob', 20], ['Carol', 30], ['Dave', 20]]

        expect: /* express the following using people and one of the methods you've just known */
        peopleNames == gap
        peopleAges == gap

        when: /* as above */
        def nameAgePairs = gap

        then:
        nameAgePairs == expectedNameAgePairs

        when: /* express nameAgePairs again with a method you've known in Lists chapter (hint: no closures needed :)) */
        nameAgePairs = gap

        then:
        nameAgePairs == expectedNameAgePairs

        when: /* again, the method to use is in this file :) */
        def ageForPersonWithName = gap

        then:
        ageForPersonWithName == [Alice: 10, Bob: 20, Carol: 30, Dave: 20]

        where:
        gap = 'gap :)'
    }

    def 'counting and summing'() {
        expect:
        [1, 2, 3].sum() == 6
        people.sum { it.age } == 80
        people.count { it.age > 20 } == 1
    }

    /* Exercise
    check how count and sum behave for maps
    */
    @Ignore
    def 'counting and summing maps'() {
        given:
        def peopleByAge = people.groupBy { it.age }

        expect:
        false //exercise
    }

    def 'grouping by'() {
        given:
        def peopleByAge = people.groupBy { it.age }

        expect:
        peopleByAge == [10: [alice], 20: [dave, bob], 30: [carol]]
    }


    def 'sort can take a closure!'() {
        expect:
        [1, 2, 3].min() == 1
        people.min { it.age } == alice
        people.min { a, b -> a.age <=> b.age } == alice
    }

    /*
    Exercise:
     sort the 'poeple' list by name without mutating it
    */
    @Ignore('exercise')
    def 'sort by name without mutating'() {
        when:
        def peopleSortedByName = 'exercise :)'

        then:
        peopleSortedByName == [alice, bob, carol, dave]
        people != peopleSortedByName
    }

    /*
    Exercise
     fill in the gaps :)
    */
    @Ignore('exercise')
    def 'sorting with compound orders'() {
        expect:
        people.sort(ageDescendingThenNameComparator) == [carol, bob, dave, alice]
        people.sort(ageThenNameOrder) == [alice, bob, dave, carol]
        people.sort(ageDescendingThenNameDescendingOrder) == [carol, dave, bob, alice]

        where:
        ageDescendingThenNameComparator = new OrderBy<Person>([ /* gap :) */ ])
        ageThenNameOrder = { 'gap :)' }
        ageDescendingThenNameDescendingOrder = { 'gap :)' }
    }

    def 'inject (a.k.a. *fold*ing or (in less general case) *reduce*ing'() {
        given:
        def plus = { a, b -> a + b }

        expect: 'injects an operator between elements of non-empty lists'
        [1, 2, 3, 4].inject(plus) == 1 + 2 + 3 + 4

        and: 'potentially empty lists'
        [].inject(0, plus) == 0
    }

    /*
    Exercise
     write a factorial function :)
    */
    @Ignore('exercise')
    @Unroll('factorial of #n should be #factorialOfN')
    def 'factorial using inject'() {
        expect:
        factorial(n) == factorialOfN

        where:
        n  | factorialOfN
        0  | 1
        1  | 1
        2  | 2
        3  | 6
        4  | 24
        5  | 120
        6  | 720
        42 | 1405006117752879898543142606244511569936384000000000
    }

    def factorial(BigInteger n) {
        '4; //chosen by a fair dice roll' // http://xkcd.com/221/
    }

    def 'asymmetric inject'() {
        given:
        def addLength = { int currentLength, String string -> currentLength + string.length() }

        expect:
        ['foo', 'bar', 'boom'].inject(0, addLength) == 10
    }

    /*
    More closure goodness in groovy:

    findAll (a.k.a. filter),
    findResult, findResults (trying not to return nulls),

    findIndexOf, findLastIndexOf, findIndexValues

    removeAll
    retainAll

    split - splits a collection in two based on a boolean condition (boolean-returning closure)
    unique - quite not obvious, read the docs :)

    */
}
