package warsjawa2012.spockogroovy

import spock.lang.Specification
import grails.spring.BeanBuilder
import spock.lang.Shared
import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext
import spock.lang.Ignore

class Ex07SpringIocWithDslSpecification extends Specification {

    def 'Spring context with XML beans config (the old way)'() {

        given:
        String springConfigurationPath = '/spring-beans.xml'

        when:
        ApplicationContext context = new ClassPathXmlApplicationContext(springConfigurationPath)
        String bean = context.getBean('aString')

        then:
        bean != null
        bean == 'Spring bean from XML'
    }




    @Ignore('exercise: add personBean definition to the spring-beans.xml')
    def 'Spring IoC with XML (the old way)'() {

        given:
        ApplicationContext context = new ClassPathXmlApplicationContext('/spring-beans.xml')

        when:
        String stringBean = context.getBean('aString')
        Person personBean = context.getBean('person')

        then:
        personBean != null
        personBean.age == 33
        personBean.name.is(stringBean)
    }




    def 'Spring context with Groovy beans config'() {

        given:
        String springBeansResourcePath = 'file:src/test/resources/SpringBeans.groovy'
        BeanBuilder bb = new BeanBuilder()
        bb.loadBeans(springBeansResourcePath)

        when:
        ApplicationContext context = bb.createApplicationContext()
        String bean = context.getBean('aString')

        then:
        bean != null
        bean == 'Spring bean from Groovy'
    }




    @Ignore('exercise: add personBean definition to the SpringBeans.groovy')
    def 'Spring beans with Groovy config'() {

        given:
        String springBeansResourcePath = 'file:src/test/resources/SpringBeans.groovy'
        BeanBuilder bb = new BeanBuilder()
        bb.loadBeans(springBeansResourcePath)
        ApplicationContext context = bb.createApplicationContext()

        when:
        String stringBean = context.getBean('aString')
        Person personBean = context.getBean('person')

        then:
        personBean != null
        personBean.age == 18
        personBean.name.is(stringBean)
    }

}
